angular.module('funcef-upload').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/upload-file.view.html',
    "<div ng-class=\"{'single-file' : singleFile}\"> <div flow-init=\"vm.params\" flow-file-success=\"vm.sucesso($file)\" flow-file-error=\"vm.erro($file)\" flow-files-submitted=\"$flow.upload()\" flow-drag-enter=\"dropClass='drag-over'\" flow-drag-leave=\"dropClass=''\" flow-drop=\"\"> <div class=\"drop {{dropClass}}\" ng-class=\"{ 'col-sm-8': singleFile }\"> <button class=\"btn btn-primary\" flow-btn=\"\"> <i class=\"fa fa-paperclip\"></i> Selecione o{{vm.params.singleFile ? '' : 's'}} arquivo{{vm.params.singleFile ? '' : 's'}} <input type=\"file\" ng-model=\"model\" name=\"files\" class=\"hide\" ng-change=\"vm.change(this)\"> </button> <span ng-if=\"!model.name\">ou arraste e solte seu{{vm.params.singleFile ? '' : 's'}} aqui...</span> <span ng-if=\"model.name\">{{model.name}}</span> </div> <div id=\"content-upload\" ng-class=\"{'row' : !singleFile, 'col-sm-4': singleFile}\"> <div ng-class=\"{'mt-15' : !singleFile}\"> <div ng-repeat=\"file in $flow.files\" class=\"gallery-box col-sm-3\" id=\"{{file.uniqueIdentifier}}\"> <div class=\"thumbnail\" ng-show=\"$flow.files.length\"> <img flow-img=\"file\" ng-if=\"{jpg:1,gif:1,png:1}[file.getExtension()]\"> <i class=\"icon-file fa fa-file-excel-o text-success\" ng-if=\"{xlsx:1,xls:1}[file.getExtension()]\"></i> <i class=\"icon-file fa fa-file-word-o text-info\" ng-if=\"{docx:1,doc:1}[file.getExtension()]\"></i> <i class=\"icon-file fa fa-file-pdf-o text-danger\" ng-if=\"{pdf:1}[file.getExtension()]\"></i> <div class=\"progress-top\" ng-class=\"{'progress-show2' : file}\"> <div class=\"progress progress-striped\" ng-class=\"{active: file.isUploading()}\"> <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"{{file.progress() * 100}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" ng-style=\"{width: (file.progress() * 100) + '%'}\"> <label class=\"sr-only\">{{file.progress()}}% Complete</label> </div> </div> <div class=\"btn-group\"> <button class=\"btn btn-xs btn-danger\" ng-click=\"file.cancel()\"> <i class=\"fa fa-trash-o\"></i> Remove </button> </div> </div> </div> <span class=\"title\" ng-if=\"!singleFile\">{{file.name}}</span> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('views/upload.modal.view.html',
    "<div class=\"modal-header\"> <h4 class=\"modal-title\"> Adicionar Arquivos </h4> </div> <div class=\"modal-body modal-body-fixed\" ng-init=\"vm.aba = 1\"> <div class=\"panel with-nav-tabs panel-default\"> <div class=\"panel-heading\"> <ul class=\"nav nav-tabs\"> <li class=\"active\"> <a data-toggle=\"tab\" ng-click=\"vm.aba = 1\"> <i class=\"fa fa-upload\"></i> Enviar arquivos </a> </li> <!--<li>\r" +
    "\n" +
    "                        <a data-toggle=\"tab\" ng-click=\"vm.aba = 2\">\r" +
    "\n" +
    "                            <i class=\"fa fa-file-text\"></i>\r" +
    "\n" +
    "                            Número do GEDOC (NUP)\r" +
    "\n" +
    "                        </a>\r" +
    "\n" +
    "                    </li>--> </ul> </div> <div class=\"panel-body\"> <div class=\"tab-content\"> <div class=\"tab-pane in active\" ng-show=\"vm.aba == 1\"> <ngf-upload-file query=\"vm.query\" target=\"vm.target\"></ngf-upload-file> </div> <!-- <div class=\"tab-pane\" ng-show=\"vm.aba == 2\">\r" +
    "\n" +
    "                        <div class=\"alert alert-info\">\r" +
    "\n" +
    "                            <i class=\"fa fa-exclamation-circle\"></i>\r" +
    "\n" +
    "                            Informe os números dos documentos no formuário abaixo!\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <form class=\"form-inline\">\r" +
    "\n" +
    "                            <div class=\"mb-10\" ng-repeat=\"numero in vm.formNups track by $index\">\r" +
    "\n" +
    "                                <div class=\"form-group form-item\">\r" +
    "\n" +
    "                                    <label class=\"control-label\">Nº do GEDOC (NUP): </label>\r" +
    "\n" +
    "                                    <input type=\"text\" name=\"{{numero.id}}\" ng-model=\"numero.numero\"\r" +
    "\n" +
    "                                           class=\"form-control\" placeholder=\"Digite o número do GDOC (NUP)\" />\r" +
    "\n" +
    "                                    <button class=\"btn btn-danger ml-5\" ng-click=\"vm.removeFormNup($index)\" ng-show=\"$index != 0\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-trash\"></i>\r" +
    "\n" +
    "                                    </button>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </form>\r" +
    "\n" +
    "                        <button class=\"btn btn-primary mt-35\" ng-click=\"vm.addNewFormNup()\">\r" +
    "\n" +
    "                            <i class=\"fa fa-plus\"></i>\r" +
    "\n" +
    "                            Adicionar Campos\r" +
    "\n" +
    "                        </button>\r" +
    "\n" +
    "                    </div> --> </div> </div> </div> </div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"vm.close()\"> <i class=\"fa fa-check\"></i> Concluir </button> </div>"
  );


  $templateCache.put('views/upload.view.html',
    "<div> <div class=\"row\"> <div class=\"col-sm-5\"> <button class=\"btn btn-primary\" ng-if=\"!disabled && buttonTop\" ng-class=\"{'pull-left' : files.length}\" ng-click=\"vm.openModalUpload()\"> <i class=\"fa fa-plus\"></i> Adicionar Arquivos </button> </div> </div> <div class=\"files-container col-sm-12 text-center\" ngf-empty=\"\" icon-empty=\"true\" padding-top=\"{{disabled ? '55px' : '15px'}}\" text-empty=\"Nenhum arquivo adicionado!\"> <ngf-file files=\"files\" ng-if=\"!template\" editar=\"!disabled\"></ngf-file> <div class=\"content\"></div> <button class=\"btn btn-default btn-add-file\" ng-if=\"!disabled && !buttonTop\" ng-class=\"{'pull-left' : files.length}\" ng-click=\"vm.openModalUpload()\"> <i class=\"fa fa-plus\"></i> Adicionar Arquivos </button> </div> </div>"
  );

}]);
