(function () {
    'use strict';

    angular.module('funcef-upload.controller').
        controller('NgfUploadController', NgfUploadController);

    NgfUploadController.$inject = ['$scope', '$uibModal'];

    /* @ngInject */
    function NgfUploadController($scope, $uibModal) {

        var vm = this;
        vm.openModalUpload = openModalUpload;

        //////////

        function openModalUpload() {
            $uibModal.open({
                templateUrl: 'views/upload.modal.view.html',
                controller: 'NgfUploadModalController',
                controllerAs: 'vm',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    query: $scope.query,
                    target: $scope.target,
                    reload: function () {
                        return $scope.reload();
                    }
                },
                link: function (scope, el, attrs, ctrl, transclude) {
                    if (attrs.template) {
                        el.find('.content').after(transclude()).remove();
                    }
                }
            });
        }
    }
})();

