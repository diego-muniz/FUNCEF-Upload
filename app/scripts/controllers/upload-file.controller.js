(function () {
    'use strict';

    angular.module('funcef-upload-file.controller').
        controller('NgfUploadFileController', NgfUploadFileController);

NgfUploadFileController.$inject = ['$scope', '$timeout', '$attrs', 'BASEPATH'];

    /* @ngInject */
    function NgfUploadFileController($scope, $timeout, $attrs, BASEPATH) {

        var vm = this;
        vm.basepath = BASEPATH;
        vm.sucesso = sucesso;
        vm.erro = erro;
        vm.params = {};

        init();

        //////////

        function init() {
            montarParamentros();
        }

        function montarParamentros() {
            vm.params = {
                target: $scope.target ? $scope.target : vm.basepath + 'Arquivo/Upload',
                query: $scope.query ? $scope.query : {},
                withCredentials: $scope.withCredentials ? $scope.withCredentials : true,
                singleFile: $scope.singleFile ? $scope.singleFile : false
            };

            if ($scope.singleFile == true) {
                vm.params.query.singleFile = true;
            }
        }

        function sucesso(file) {
            $timeout(function () {
                angular.element('#' + file.uniqueIdentifier).addClass('complete');
            }, 1000);
        }

        function erro(file) {
            sucesso(file);
            var local = '#' + file.uniqueIdentifier;
            local += $attrs.singleFile ? '' : ' .thumbnail';
            angular.element(local).append('<div class="alert alert-danger"><b>Erro!</b> Não foi possível enviar o arquivo!</div>');
        }
    };
})();