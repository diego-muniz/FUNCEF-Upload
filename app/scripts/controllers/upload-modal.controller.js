(function () {
    'use strict';

    angular.module('funcef-upload-modal.controller').
        controller('NgfUploadModalController', NgfUploadModalController);

     NgfUploadModalController.$inject = ['$uibModalInstance', '$scope', '$timeout', 'BASEPATH', 'query', 'target', 'reload'];

    /* @ngInject */
    function NgfUploadModalController($uibModalInstance, $scope, $timeout, BASEPATH, query, target, reload) {

        var vm = this;
        vm.basepath = BASEPATH;
        vm.formNups = [];
        vm.close = close;
        vm.addNewFormNup = addNewFormNup;
        vm.removeFormNup = removeFormNup;
        vm.query = query;

        init();

        //////////

        function init() {
            addNewFormNup();
        }

        function close() {
            $uibModalInstance.dismiss('cancel');
            reload();
        }

        function addNewFormNup() {
            var newItemNup = vm.formNups.length + 1;
            vm.formNups.push({ 'id': newItemNup, Nome: '' });
        };

        function removeFormNup(index) {
            vm.formNups.splice(index, 1);
        };
    };
})();