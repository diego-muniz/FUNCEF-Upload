﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Upload
    * @version 1.0.0
    * @Componente Upload de arquivos
    */
    angular.module('funcef-upload.controller', []);
    angular.module('funcef-upload-file.controller', []);
    angular.module('funcef-upload-modal.controller', []);
    angular.module('funcef-upload.directive', []);
    angular.module('funcef-upload-file.directive', []);
    angular.module('funcef-upload.configuration', []);
    

    angular
    .module('funcef-upload', [
        'funcef-upload.directive', 
        'funcef-upload.configuration',
        'funcef-upload.controller',

        'funcef-upload-file.controller', 
        'funcef-upload-file.directive',

        'funcef-upload-modal.controller',
        
        'ui.bootstrap',
        'flow'
    ]);
})();