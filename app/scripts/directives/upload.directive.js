(function () {
    'use strict';

    angular
      .module('funcef-upload.directive')
      .directive('ngfUpload', NgfUpload);

    NgfUpload.$inject = ['$compile'];

    /* @ngInject */
    function NgfUpload() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/upload.view.html',
            controller: 'NgfUploadController',
            controllerAs: 'vm',
            scope: {
                query: '=',
                files: '=',
                disabled: '=',
                reload: '&',
                target: '=',
                template: '=',
                buttonTop: '=',
                filterModel: '='
            },
            link: function (scope, el, attrs, ctrl, transclude) {
                if (attrs.template == 'true') {
                    el.find('.content').append(transclude());
                }
            }
        };
    }
})();