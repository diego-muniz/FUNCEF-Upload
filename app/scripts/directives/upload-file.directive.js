﻿(function () {
    'use strict';

    angular
      .module('funcef-upload-file.directive')
      .directive('ngfUploadFile', NgfUploadFile);

    /* @ngInject */
    function NgfUploadFile() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/upload-file.view.html',
            controller: 'NgfUploadFileController',
            controllerAs: 'vm',
            scope: {
                files: '=',
                query: '=',
                target: '=',
                singleFile: '=',
                model: '='
            }
        };
    }
})();