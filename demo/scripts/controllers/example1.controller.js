﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = [];

    /* @ngInject */
    function Example1Controller() {
        var vm = this;

        vm.listar = listar;
        vm.lista = {};
        vm.query = {id: 1}

		init();

        function init(){
        	listar();
        }


        function listar(){

        	vm.lista = [
        		{
        		 Id: 1,
        		 Nome: "Arquivo.xlxs",
        		 Extensao: "xlxs",
        		 Tamanho: 115922,
        		 Link: "C:\Arquivo.xlsx"
        		}]; 
        }
    }
}());

